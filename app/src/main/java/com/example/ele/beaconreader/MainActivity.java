package com.example.ele.beaconreader;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bettina Finzel on 07.11.2015.
 */
public class MainActivity extends AppCompatActivity {
    //Delimiter used in file
    public static final String COMMA_DELIMITER = ",";

    //new line
    private static final String NEW_LINE_SEPARATOR = "\n";

    //file header
    private static final String FILE_HEADER = "UUID,MAJOR,MINOR";

    //TODO: add needed fields
    private ArrayList<Beacon> beaconList=new ArrayList<Beacon>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO: get intent
    }

    @Override
    protected void onResume() {
        super.onResume();
        //TODO: get intent and add beacons to List
        //showBeaconsInLinearLayout();
        try {

            Bundle bundle = getIntent().getExtras();

            for(int i=0;i<bundle.getInt("count");i++) {
                String uuid = bundle.getString("uuid"+i);
                int major = bundle.getInt("major"+i);
                int minor = bundle.getInt("minor"+i);
                beaconList.add(new Beacon(uuid, major, minor));
                Log.d("INFO", "MainActivity:OnResume:beaconList:Add: " + uuid + " " + major + " " + minor);
            }



        }
        catch(Exception e)
        {
            Log.d("INFO", e.toString());
        }
        showBeaconsInLinearLayout();
    }

    private void showBeaconsInLinearLayout() {
        //TODO: implement this
        LinearLayout collectedBeaconsView = (LinearLayout) findViewById(R.id.collected_beacons);

        int i = 1;
        int size = beaconList.size();
        for (Beacon beacon : beaconList) {
            LinearLayout container = new LinearLayout(this);
            container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            container.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout leftDiv = new LinearLayout(this);
            leftDiv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
            leftDiv.setOrientation(LinearLayout.VERTICAL);
            leftDiv.setPadding(5, 5, 5, 5);

            LinearLayout rightDiv = new LinearLayout(this);
            rightDiv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
            rightDiv.setOrientation(LinearLayout.VERTICAL);
            rightDiv.setPadding(5, 5, 5, 5);

            if (i < size || size == 1) {
                TextView uuidA = new TextView(this);
                uuidA.setText("UUID: " + beacon.getUUID());

                TextView majorA = new TextView(this);
                majorA.setText("MAJOR: " + beacon.getMajor());

                TextView minorA = new TextView(this);
                minorA.setText("MINOR: " + beacon.getMinor());

                leftDiv.addView(uuidA);
                leftDiv.addView(majorA);
                leftDiv.addView(minorA);
            }

            if (i < size) {
                TextView uuidB = new TextView(this);
                uuidB.setText("UUID: " + beaconList.get(i).getUUID());

                TextView majorB = new TextView(this);
                majorB.setText("MAJOR: " + beaconList.get(i).getMajor());

                TextView minorB = new TextView(this);
                minorB.setText("MINOR: " + beaconList.get(i).getMinor());

                rightDiv.addView(uuidB);
                rightDiv.addView(majorB);
                rightDiv.addView(minorB);
            }
            container.addView(leftDiv);
            container.addView(rightDiv);

            collectedBeaconsView.addView(container);
            i++;
        }
    }

    //Do not change this!
    protected void writeBeaconSimulationFile(){

        //Create new beacon objects
        Beacon beacon1 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,1);
        Beacon beacon2 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,2);
        Beacon beacon3 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,3);
        Beacon beacon4 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,4);
        Beacon beacon5 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,5);
        Beacon beacon6 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,8);
        Beacon beacon7 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,9);
        Beacon beacon8 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,10);
        Beacon beacon9 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,10);
        Beacon beacon10 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,9);
        Beacon beacon11 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,8);
        Beacon beacon12 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,5);
        Beacon beacon13 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,4);
        Beacon beacon14 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,3);
        Beacon beacon15 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,2);
        Beacon beacon16 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,1);

        //Create a new list of beacons objects
        ArrayList<Beacon> beacons = new ArrayList<Beacon>();
        beacons.add(beacon1);
        beacons.add(beacon2);
        beacons.add(beacon3);
        beacons.add(beacon4);
        beacons.add(beacon5);
        beacons.add(beacon6);
        beacons.add(beacon7);
        beacons.add(beacon8);
        beacons.add(beacon9);
        beacons.add(beacon10);
        beacons.add(beacon11);
        beacons.add(beacon12);
        beacons.add(beacon13);
        beacons.add(beacon14);
        beacons.add(beacon15);
        beacons.add(beacon16);
        beacons.add(beacon15);
        beacons.add(beacon14);
        beacons.add(beacon3);
        beacons.add(beacon2);
        beacons.add(beacon1);


        try{
            FileOutputStream testFile = openFileOutput("Beacons.txt", Context.MODE_APPEND);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(testFile);
            outputStreamWriter.append(FILE_HEADER.toString());
            outputStreamWriter.append(NEW_LINE_SEPARATOR);

            for (Beacon beacon : beacons) {
                outputStreamWriter.append(String.valueOf(beacon.getUUID()));
                outputStreamWriter.append(COMMA_DELIMITER);
                outputStreamWriter.append(String.valueOf(beacon.getMajor()));
                outputStreamWriter.append(COMMA_DELIMITER);
                outputStreamWriter.append(String.valueOf(beacon.getMinor()));
                outputStreamWriter.append(NEW_LINE_SEPARATOR);
            }

            outputStreamWriter.close();
        }
        catch (IOException ex){
            Log.d("INFO", ex.getMessage());
        }
    }

    //TODO: button starts the service
    public void startServiceByButtonClick(View v) {
        //TODO: Get user input
        EditText editText = (EditText) findViewById(R.id.interval);
        long seconds = 0;

        try{
            seconds = Long.valueOf(editText.getText().toString());
            //Do not change this!
            File dir = getFilesDir();
            File file = new File(dir, "Beacons.txt");
            boolean deleted = file.delete();

            //this method writes the file containing simulated beacon data
            writeBeaconSimulationFile();

            //TODO: Service is started via intent
            Intent intent=new Intent(MainActivity.this, ServiceImpl.class);
            intent.putExtra("seconds", seconds);

            this.startService(intent);
            Log.d("INFO", "startServiceByButtonClick: Service Started");
        } catch(Exception e){
            Toast.makeText(this, "You need to specify an interval number", Toast.LENGTH_LONG).show();
        }
    }

    //TODO: stop service
    public void stopServiceByButtonClick(View v) {
        //implement this
        Log.d("INFO", "stopServiceByButtonClick: Stopping Service");
        this.stopService(new Intent(MainActivity.this, ServiceImpl.class));

    }

}
