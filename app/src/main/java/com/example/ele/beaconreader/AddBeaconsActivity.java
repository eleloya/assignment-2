package com.example.ele.beaconreader;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ele on 01/12/15.
 */
public class AddBeaconsActivity  extends AppCompatActivity {
    public static ArrayList<Beacon> beaconList= new ArrayList<Beacon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("INFO", "AddBeaconsActivity:OnCreate: Started");


        Bundle bundle = getIntent().getExtras();

        for(int i=0;i<bundle.getInt("count");i++) {
            String uuid = bundle.getString("uuid"+i);
            int major = bundle.getInt("major"+i);
            int minor = bundle.getInt("minor"+i);
            beaconList.add(new Beacon(uuid, major, minor));
            Log.d("INFO", "AddBeaconsActivity:Oncreate:beaconList:Add: " + uuid + " " + major + " " + minor);
        }

        setContentView(R.layout.activity_add_beacons);

        displayBeacons();

    }

    public void displayBeacons() {
        TableLayout table = (TableLayout) findViewById(R.id.addBeacons_table);

        //Width, height and margin parameters
        TableRow.LayoutParams tlparams = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tlparams.setMargins(1,1,1,1);

        //Weight Parameters
        TableRow.LayoutParams w1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.FILL_PARENT, 0.6f);
        TableRow.LayoutParams w2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.FILL_PARENT, 0.2f);
        TableRow.LayoutParams w3 = new TableRow.LayoutParams(0, TableRow.LayoutParams.FILL_PARENT, 0.2f);


        for (Beacon beacon : beaconList) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(tlparams);

            TextView uuid = new TextView(this);
            uuid.setText(beacon.getUUID());
            uuid.setBackgroundColor(Color.rgb(220, 220, 220));
            uuid.setTextColor(Color.rgb(0, 0, 0));
            uuid.setPadding(10, 10, 10, 10);
            uuid.setGravity(Gravity.LEFT);
            uuid.setMaxWidth(20);
            uuid.setLayoutParams(w1);
            tableRow.addView(uuid);


            TextView major = new TextView(this);
            major.setText(""+ beacon.getMajor());
            major.setBackgroundColor(Color.rgb(220, 220, 220));
            major.setTextColor(Color.rgb(0, 0, 0));
            major.setPadding(10, 10, 10, 10);
            major.setGravity(Gravity.LEFT);
            major.setLayoutParams(w2);
            tableRow.addView(major);

            TextView minor = new TextView(this);
            minor.setText("" + beacon.getMinor());
            minor.setBackgroundColor(Color.rgb(220, 220, 220));
            minor.setTextColor(Color.rgb(0, 0, 0));
            minor.setPadding(10, 10, 10, 10);
            minor.setGravity(Gravity.LEFT);
            minor.setLayoutParams(w3);
            tableRow.addView(minor);

            table.addView(tableRow);
        }
    }

    public void addBeaconsByButtonClick(View view){
        Intent mIntent =new Intent(AddBeaconsActivity.this, MainActivity.class);

        mIntent.putExtra("count",beaconList.size());


        for(int j=0;j<beaconList.size();j++){
            mIntent.putExtra("uuid" +j, beaconList.get(j).getUUID());
            mIntent.putExtra("minor"+j, beaconList.get(j).getMajor());
            mIntent.putExtra("major"+j, beaconList.get(j).getMinor());
        }

        beaconList.clear();

        this.startActivity(mIntent);
    }
}
