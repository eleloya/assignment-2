package com.example.ele.beaconreader;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bettina Finzel on 11.11.2015.
 */
public class ServiceImpl extends IntentService {

    private long seconds;
    private boolean hack_stop = false;
    //TODO: include needed fields
    private BufferedReader inputReader;
    public static ArrayList<Beacon> beaconList=new ArrayList<Beacon>();



    public ServiceImpl() {
        super("ServiceImpl");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("INFO", "onHandleIntent: Service Started");

        beaconList.clear();

        //TODO: get the seconds from intent
        seconds = intent.getExtras().getLong("seconds");

        setupInputReader();

        Log.d("INFO", "onHandleIntent: Service has a " + seconds + " interval");


        //how long the service should sleep, in milliseconds
        long millis = seconds * 1000;
        while (true) {
            if(hack_stop)
                break;
            try {
                Beacon beacon = scanBeacon();

                if(beacon != null){
                    //TODO: add beacons to the List of scanned beacons
                    beaconList.add(beacon);

                    //TODO: notification
                    android.support.v4.app.NotificationCompat.Builder notificationBuilder =
                            new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.drawable.beacon)
                                    .setContentTitle("New Beacon found")
                                    .setContentText(beacon.toString());

                    //TODO: intent to AddBeaconsActivity
                    Intent mIntent =new Intent(ServiceImpl.this, AddBeaconsActivity.class);

                    mIntent.putExtra("count", ServiceImpl.beaconList.size());

                    for(int j=0;j<beaconList.size();j++){
                        mIntent.putExtra("uuid" +j, beaconList.get(j).getUUID());
                        mIntent.putExtra("minor"+j, beaconList.get(j).getMajor());
                        mIntent.putExtra("major"+j, beaconList.get(j).getMinor());
                    }


                    //build intent to switch to activity on click
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

                    //adds the back stack for the Intent (not the intent itself)
                    stackBuilder.addParentStack(AddBeaconsActivity.class);

                    //adds the intent that starts and puts the activity to the top of the stack
                    //TODO: uncomment this and insert the above created intent as input
                    stackBuilder.addNextIntent(mIntent);

                    //PendingIntent waits for an event
                    PendingIntent scanResultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    notificationBuilder.setContentIntent(scanResultPendingIntent);
                    Notification notification = notificationBuilder.build();
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(1, notification);
                }

                Log.d("INFO","onHandleIntent: Sleeping for " + seconds + " seconds");
                //TODO: put the service to sleep
                Thread.sleep(millis);

            } catch (InterruptedException iEx) {
                Log.d("INFO", iEx.getMessage());
            }
        }

    }

    private void setupInputReader() {
        try {
            //TODO: read the file "Beacon.txt"
            Log.d("INFO", "setupInputReader: Started");
            FileInputStream beaconFile = openFileInput("Beacons.txt");
            inputReader = new BufferedReader(new InputStreamReader(beaconFile));

            //read the header in advance to exclude it from the output
            String header = new String();
            try {
                header = inputReader.readLine();

            } catch (IOException e) {
                Log.d("INFO", "setupInputReader: IOException while trying to read header");
            }

            Log.d("INFO","setupInputReader:  Header: " + header);

        } catch (FileNotFoundException fnfEx) {
            Log.d("INFO", "setupInputReader: FileNotFound beacons.txt");
        } catch (IOException ioEx) {
            Log.d("INFO", "setupInputReader: IOException at setupInputReader");
        }
    }

    private Beacon scanBeacon() {
        String line = null;
        try {
            line = inputReader.readLine();
            if (line != null) {
                Log.d("INFO", "scanBeacon: line=" + line);

                //TODO: split one row into the beacon components uuid, major and minor
                //create a new beacon and return it
                String csv[]=line.split(",",3);
                Beacon beacon = new Beacon(csv[0],Integer.parseInt(csv[1]),Integer.parseInt(csv[2]));
                return beacon;
            }

        } catch (IOException ioEx) {
            Log.d("INFO", "scanBeacon: " + ioEx.getMessage());
        }
        return null;
    }

    public void onDestroy() {
        //TODO: implement this
        hack_stop = true;
        Log.d("INFO", "onDestroy: Service stopped");
        Toast.makeText(this, "Scan stopped", Toast.LENGTH_SHORT).show();

        super.onDestroy();
        stopSelf();
    }
}
